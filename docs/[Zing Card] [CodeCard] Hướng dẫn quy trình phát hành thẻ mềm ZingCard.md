#### [Zing Card] [CodeCard] Hướng dẫn quy trình phát hành thẻ mềm ZingCard

July 4, 2019 [ZingCard Tool](https://wiki.mto.zing.vn/category/zingcard-tool/)

*Quy trình phát hành thẻ mềm ZingCard bao gồm 2 giai đoạn tuần tự, bài viết sẽ
hướng dẫn user từng giai đoạn cụ thể.*

**I. Giai đoạn tạo yêu cầu và xuất file dữ liệu thẻ mềm**

![](media/92b1f9b3e122e3bd6593a966f7738111.png)

1. **Distributor add request:**

Step 1: Đi tới trang ZingCard Tool

Step 2: Đăng nhập bằng Distributor account

Step 3: Click tab Card Distribution » Distribution Request List

Step 4: Click Add Request

Màn hình sẽ được hiển thị như sau:

![](media/ed6ddc9d404f58ab55d35accfd23a489.png)

Step 5:

\+ Mục **Distribution Type\*** : Chọn “Code”

\+ **Agent:** chọn Agent sẽ nhận SMS Password

\+ **Purpose\***: Có thể chọn 1 option bất kỳ trong list

\+ **Quantity:** Chọn số lượng thẻ tương ứng với từng mệnh giá (Zingcard Tool
cho phép số lượng thẻ tối đa trên mỗi file là 50000)

\+ **Number of File:** Chọn số lượng file lưu trữ thẻ tương ứng từng mệnh giá.

Ví dụ: Mệnh giá 10k, Quantity: 1, Number of file 1 nghĩa là request 1 card 10k,
file lưu trữ là 1.

Step 6: Click Save. Sau khi Distributor click Save, Distributor Leader sẽ nhận
được 1 email thông báo có đơn hàng mới và yêu cầu duyệt.

2. **Distributor Leader duyệt đơn hàng (level 1)**

Step 1: Distributor Leader click vào link trên email để đi tới Zingcard Tool và
phê duyệt hoặc từ chối đơn hàng. Hoặc đi tới Zingcard Tool \> login bằng
Distributor Leader account \> Card Distribution » Accept/Decline Distribution
List

Step 2a: Distributor Leader từ chối đơn hàng:

Click vào ID đơn hàng đó \> click Decline

Sau khi Distributor từ chối duyệt đơn hàng:

\+ Status của đơn hàng sẽ được chuyển sang Decline.

\+ Hệ thống gửi mail cho Distributor báo đơn hàng đã bị từ chối.

Step 2b: Distributor Leader chấp thuận duyệt đơn hàng:

Click vào ID đơn hàng đó \> Click Accept.

Sau khi Distributor Leader chấp thuận duyệt đơn hàng:

\+ Status của đơn hàng đó sẽ được chuyển qua Approve.

\+ Hệ thống gửi mail cho Distributor báo đơn hàng đã được chấp thuận duyệt.

\+ Hệ thống gửi mail cho CMT Leader thông báo có đơn hàng mới và yêu cầu duyệt
đơn.

3. **CMT Leader duyệt đơn hàng (level 2)**

Step 1: CMT Leader click vào link trên email để đi tới Zingcard Tool và phê
duyệt hoặc từ chối đơn hàng. Hoặc đi tới Zingcard tool \> Login bằng CMT Leader
account \> Click Card Distribution » Approve/Reject Distribution List.

Step 2a: CMT Leader từ chối duyệt đơn hàng:

\+ Status đơn hàng sẽ được chuyển qua Reject

\+ Hệ thống gửi email cho Distributor thông báo đơn hàng đã bị từ chối

\+ Hệ thông gửi email cho Distributor Leader thông báo đơn hàng đã bị từ chối

Step 2b: CMT Leader đồng ý duyệt đơn hàng:

\+ Status đơn hàng sẽ được chuyển qua Approve

\+ Hệ thống gửi email cho Distributor thông báo đơn hàng đã được Approve

\+ Hệ thông gửi email cho Distributor Leader thông báo đơn hàng đã được Approve

\+ Agent nhận SMS chứa Password giải nén file thẻ

\+ Distributor nhận mail yêu cầu xác nhận đã nhận SMS chứa password

4. **Distributor xác nhận đã nhận SMS chứa password**

Step 1: Distributor click vào link trên email xác nhận đã nhận SMS chứa
password. \> User được điều hướng tới Zingcard Tool với message xác nhận thành
công.

Sau khi Distributor xác nhận đã nhận SMS chứa password, hệ thống sẽ gửi mail
chứa file download card tới Distributor.

5. **Distributor download card file**

Step 1: Distributor click vào link download card file trên email

Step 2: Distributor click vào link ” Please click here to download card files”
link trên Zingcard tool site. Sau khi user click vào link download:

\+ Nút confirm được hiển thị ngay dưới link download.

\+ File thẻ được tải về máy dạng nén được khóa bằng password đã gửi cho Agent

Step 3: Distributor click Confirm để xác nhận đã download thành công card file.
Sau khi user click Confirm, link download thẻ sẽ bị vô hiệu hóa. User không thể
download lại.

*\*Note: Distributor đã download card file nhưng không click Confirm: đơn hàng
sẽ không được hiển thị trong phần Update Status.*

**II. Giai đoạn kích hoạt thẻ mềm**

**1. Distributor Request Card Activation**

Step 1: Đi tới Zingcard Tool\> Login bằng Distributor account

Step 2: Click Card Activation » Add Request Activation

Step 3: Nhập thông tin vào form.

\+ Đối với **PO Number, PO Number TS, Release Number, Release Number TS:** có
thể nhập chuỗi ký tự bất kỳ

\+ Nhập thông tin card

Step 4: Click Save. Sau khi Distributor save thành công yêu cầu kích hoạt thẻ:

\+ Yêu cầu kích hoạt thẻ sẽ được hiển thị trên Request Card Activation với
status là New

\+ CMT leader nhận được mail thông báo có yêu cầu kích hoạt thẻ cần được duyệt

2. Duyệt yêu cầu kích hoạt thẻ:

Step 1: CMT Leader click vào link trên email hoặc truy cập Zingcard Tool \>
Login bằng CMT account \> Click Card Activation (NEW) » Approve/Reject Request
Activation.

Step 2a: CMT Leader từ chối duyệt yêu cầu kích hoạt thẻ.

Sau khi CMT Leader click Reject:

\+ Request change status từ New sang Rejected

\+ Distributor nhận được mail thông báo yêu cầu kích hoạt đã bị từ chối

\+ Card serial nằm trong request này không thể sử dụng

Step 2b: CMT Leader chấp thuận yêu cầu kích hoạt thẻ.

Sau khi CMT click Approve&Activate:

\+ Request change status từ New sang Approved

\+ Distributor nhận được mail thông báo yêu cầu kích hoạt thẻ đã được duyệt

\+ Card serial nằm trong request này có thể sử dụng
