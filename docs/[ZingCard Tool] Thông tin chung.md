[ZingCard Tool] Thông tin chung

  July 1, 2019   *ZingCard Tool*

**I. Các environments/ links hỗ trợ:**

 ZingCard Tool có 2 environments là Sandbox và Production.

1. Sandbox: –  server cũ: <http://sandbox.zingcardv2new.vng.com.vn/>

Với môi trường này, user có thể truy cập mà không cần adhost hay VPN và thực
hiện test tất cả các functions .

2. Production:

Production – server mới : http://zingcard.pay.zing.vn/

Production – server cũ: https://zingcardv2new.vng.com.vn

Đối với cả 2 link production bên trên QC không có quyền truy cập và test trên 2
links đó.

**II. Các account sử dụng để đăng nhập Sandbox:**

– nhanptt4: role Distributor

– nhutbm: role Distribution Leader

– manhtt: role CMT Leader

– thientp: role Administrator

\*Lưu ý: Những account này có thể bị thay đổi permission và role tùy từng thời
điểm. Nếu cần truy cập sandbox mà chưa có account, có thể liên hệ anh ThiệnTP để
nhờ tạo.
