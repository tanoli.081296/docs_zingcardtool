#### [Zing Card] [Print Card] Hướng dẫn quy trình phát hành thẻ cứng ZingCard

July 4, 2019 [ZingCard Tool](https://wiki.mto.zing.vn/category/zingcard-tool/)

*Quy trình phát hành thẻ cứng ZingCard bao gồm 4 giai đoạn tuần tự, bài viết sẽ
hướng dẫn user từng giai đoạn cụ t*hể.

**I. Giai đoạn tạo yêu cầu và xuất file dữ liệu thẻ cào**

![](media/0cba7d23e3e95b25dc022cc6829a589f.png)

**1. Distributor add request:**

Step 1: Đi tới trang ZingCard Tool

Step 2: Đăng nhập bằng Distributor account

Step 3: Click tab Card Distribution » Distribution Request List

Step 4: Click Add Request

Màn hình sẽ được hiển thị như sau:

![](media/ed6ddc9d404f58ab55d35accfd23a489.png)

Step 5:

\+ **Distribution Type\*** : Chọn “Print”

\+ **Purpose\***: Có thể chọn 1 option bất kỳ trong list

\+ **Quantity:** Chọn số lượng thẻ tương ứng với từng mệnh giá

\+ **Number of File:** Chọn số lượng file lưu trữ thẻ tương ứng từng mệnh giá.

Ví dụ: Mệnh giá 10k, Quantity 1, Number of file 1 nghĩa là request 1 card 10k,
file lưu trữ là 1.

Step 6: Click Save. Sau khi Distributor click Save, Distributor Leader sẽ nhận
được 1 email thông báo có đơn hàng mới và yêu cầu duyệt.

**2. Distributor Leader duyệt đơn hàng (level 1)**

Step 1: Distributor Leader click vào link trên email để đi tới Zingcard Tool và
phê duyệt hoặc từ chối đơn hàng. Hoặc đi tới Zingcard Tool \> login bằng
Distributor Leader account \> Card Distribution » Accept/Decline Distribution
List

Step 2a: Distributor Leader từ chối đơn hàng:

Click vào ID đơn hàng đó \> click Decline. Sau khi Distributor từ chối duyệt đơn
hàng:

\+ Status của đơn hàng sẽ được chuyển sang Decline.

\+ Hệ thống gửi mail cho Distributor báo đơn hàng đã bị từ chối.

Step 2b: Distributor Leader chấp thuận duyệt đơn hàng:

Click vào ID đơn hàng đó \> Click Accept. Sau khi Distributor Leader chấp thuận
duyệt đơn hàng:

\+ Status của đơn hàng đó sẽ được chuyển qua Approve.

\+ Hệ thống gửi mail cho Distributor báo đơn hàng đã được chấp thuận duyệt.

\+ Gửi mail cho CMT Leader thông báo có đơn hàng mới và yêu cầu duyệt đơn.

**3. CMT Leader duyệt đơn hàng (level 2)**

Step 1: CMT Leader click vào link trên email để đi tới Zingcard Tool và phê
duyệt hoặc từ chối đơn hàng. Hoặc đi tới Zingcard tool \> Login bằng CMT Leader
account \> Click Card Distribution » Approve/Reject Distribution List.

Step 2a: CMT Leader từ chối duyệt đơn hàng:

Status đơn hàng sẽ được chuyển qua Reject

\+ Hệ thống gửi email cho Distributor thông báo đơn hàng đã bị từ chối

\+ Hệ thông gửi email cho Distributor Leader thông báo đơn hàng đã bị từ chối

Step 2b: CMT Leader đồng ý duyệt đơn hàng:

\+ Status đơn hàng sẽ được chuyển qua Approve

\+ Hệ thống gửi email cho Distributor thông báo đơn hàng đã được Approve

\+ Hệ thông gửi email cho Distributor Leader thông báo đơn hàng đã được Approve

\+ Password Keeper nhận SMS chứa Password giải nén file thẻ

\+ Passwork Keeper nhận mail yêu cầu xác nhận đã nhận SMS chứa password

**4. Password Keeper xác nhận đã nhận SMS chứa password**

Step 1: Passwork Keeper click vào link trên email xác nhận đã nhận SMS chứa
password. \> User được điều hướng tới Zingcard Tool với message xác nhận thành
công.

Sau khi Password Keeper xác nhận đã nhận SMS chứa password, hệ thống sẽ gửi mail
chứa file download card tới Card Keeper.

**5. Card Keeper download card file**

Step 1: Card Keeper click vào link download card file trên email

Step 2: Card Keeper click vào link ” Please click here to download card files”
link trên Zing Card tool site. Sau khi user click vào link download:

\+ Nút confirm được hiển thị ngay dưới link download.

\+ File thẻ được tải về máy dạng nén và được khóa bằng password đã gửi cho
Password Keeper.

Step 3: Card Keeper click Confirm để xác nhận đã download thành công card file.
Sau khi user click Confirm, link download thẻ sẽ bị vô hiệu hóa. User không thể
download lại.

*\*Note: Card Keeper đã download card file nhưng không click Confirm: đơn hàng
sẽ không được hiển thị trong phần Update Status.*

**II. Giai đoạn tương tác với nhà in và nhập kho thẻ**

![](media/997ea880c9390e8c65c7439eda2143dd.png)

**1. Card Keeper update card status**

Step 1: Card Keeper Click Card Distribution » Update Card Status

Step 2: Chọn ID đơn hàng trong Distribution list

Step 3: Click Distributed To Printing-House \> Hệ thống trả về message update
status thành công

**2. Warehouse Keeper add receipt note**

Step 1: Warehouse Keeper click Card Distribution » Receipt Note

Step 2: Click Add Receipt Note

Màn hình Add Receipt Note:

![](media/d5fa1b5c054fd31b936f6f1518d130b5.png)

**Deliverer \*:** Nhập tên người gửi

**Storehouse \*:** chọn kho sẽ nhập card. Store House chỉ hiển thị kho mà
WareHouse Keeper đó quản lý.

**PO Number \*:** nhập 1 chuỗi ký tự bất kỳ

**Receipt From \*:** chọn 1 kho bất kỳ

**Release Number \*:** Nhập 1 chuỗi ký tự bất kỳ

**Card info**: Nhập thông tin thẻ. Thông tin này được lấy từ file card sau khi
download file thẻ và giải nén.

Step 3: Save Receipt Note

**III. Giai đoạn tạo yêu cầu xuất kho phôi thẻ cào**

**1. Distributor Add Delivery Request**

Step 1: Đi tới Zingcard Tool site \> Login bằng Distributor account

Step 2: Click Card Delivery » Delivery Request List » Add Delivery Request

Màn hình Add Delivery Request:

![](media/c834e0a9121214923963280b5cf92adc.png)

**Storehouse\*:** Chọn StoreHouse giống trên step 2 – mục 2 – phần II.

**Delivery To \*:** Chọn 1 kho bất kỳ

**Card Type – Quantity:** Chọn Mệnh giá card và số lượng card tương ứng

Mệnh giá và số lượng card tương ứng với mục 1 – Phần I.

Step 3: Save

Sau khi Distributor save thành công Delivery Request. Hệ thống gửi mail cho
Distributor Leader về yêu cầu xuất kho thẻ cần duyệt.

**2. Distributor Leader duyệt yêu cầu xuất kho**

Step 1: Distributor Leader click vào link trên email yêu cầu duyệt xuất kho.
Hoặc đi tới Zingcard Tool \> Login bằng Distributor Leader account \> Click Card
Delivery » Approve/Reject Delivery List

Step 2a: Distributor Leader từ chối yêu cầu xuất kho.

Sau khi Distributor Leader click Reject trên đơn yêu cầu xuất kho:

\+ Distributor nhận được email thông báo yêu cầu xuất kho đã bị từ chối

\+ Status yêu cầu xuất kho chuyển sang Rejected

Step 2b: Distributor Leader chấp thuận yêu cầu xuất kho.

Sau khi Distributor Leader click Approve trên đơn yêu cầu xuất kho:

\+ Distributor nhận được email thông báo yêu cầu xuất kho đã được duyệt

\+ Status yêu cầu xuất kho chuyển sang Approved

\+ Warehouse Keeper nhận mail thông tin xuất kho thẻ

3. **Warehouse Keeper xuất kho thẻ chưa active**

Step 1: Đi tới Zingcard Tool \> Login bằng Warehouse Keeper account

Step 2: Click Card Distribution » Delivery Note

Step 3: Chọn mã đơn hàng \> Assign Serial \> Nhập thông tin vào form

Step 4: Click Save

**IV. Quy Trình Kích Hoạt Thẻ Cào**

**1. Distributor Request Card Activation**

Step 1: Đi tới Zingcard Tool\> Login bằng Distributor account

Step 2: Click Card Activation » Add Request Activation

Step 3: Nhập thông tin vào form.

\+ Đối với **PO Number, PO Number TS, Release Number, Release Number TS:** có
thể nhập chuỗi ký tự bất kỳ

\+ Nhập thông tin card

Step 4: Click Save. Sau khi Distributor save thành công yêu cầu kích hoạt thẻ:

\+ Yêu cầu kích hoạt thẻ sẽ được hiển thị trên Request Card Activation với
status là New

\+ CMT leader nhận được mail thông báo có yêu cầu kích hoạt thẻ cần được duyệt

2. Duyệt yêu cầu kích hoạt thẻ:

Step 1: CMT Leader click vào link trên email hoặc truy cập Zingcard Tool \>
Login bằng CMT account \> Click Card Activation (NEW) » Approve/Reject Request
Activation.

Step 2a: CMT Leader từ chối duyệt yêu cầu kích hoạt thẻ.

Sau khi CMT Leader click Reject:

\+ Request change status từ New sang Rejected

\+ Distributor nhận được mail thông báo yêu cầu kích hoạt đã bị từ chối

\+ Card serial nằm trong request này không thể sử dụng

Step 2b: CMT Leader chấp thuận yêu cầu kích hoạt thẻ.

Sau khi CMT click Approve&Activate:

\+ Request change status từ New sang Approved

\+ Distributor nhận được mail thông báo yêu cầu kích hoạt thẻ đã được duyệt

\+ Card serial nằm trong request này có thể sử dụng

\+ Kết thúc quy trình phát hành thẻ cứng Zing.
